# -*- coding: utf-8 -*-
"""
Created on Tue Mar  3 12:03:45 2020

@author: mi
"""

import ephem
import numpy as np
import matplotlib.pyplot as plt

from ephem import stars
from ephem import FixedBody
from ephem import Angle

stars = stars.stars.items()
def sky_to_image(sky_data):
    _sky_data_xy = []
    minmax = {'x':[0,0], 'y':[0, 0]}
    for b, az, el in sky_data:
        x = (90-el)*np.cos(np.pi/2-np.radians(az))
        y = -(90-el)*np.sin(np.pi/2-np.radians(az))
        _sky_data_xy.append([b, x, y])
        minmax['x'][0] = min(minmax['x'][0], x)
        minmax['x'][1] = max(minmax['x'][1], x)
        minmax['y'][0] = min(minmax['y'][0], y)
        minmax['y'][1] = max(minmax['y'][1], y)
    for star in _sky_data_xy:
        star[1] -= minmax['x'][0]
        star[2] -= minmax['y'][0]
    return _sky_data_xy
    

def get_sky(observer, alt_min = 0, top_best = 200):
    sky = {'el': [], 'az':[], 'bright':[]}
    for _, star in stars:
        star.compute(observer)
        if star.alt > 0 and np.degrees(star.alt)>alt_min:
            sky['az'].append(star.az)
            sky['el'].append(np.degrees(star.alt))
            sky['bright'].append(star.mag)
    min_bright = min(sky['bright'])
    max_bright = max(sky['bright'])
  #  sky_data = [(-10,0,90)]+[(a, np.degrees(b), c) for a, b, c in zip(sky['bright'], sky['az'], sky['el'])]
    sky_data = [(-10,0,90)]+[(a, np.degrees(b), c) for a, b, c in zip(sky['bright'], sky['az'], sky['el'])]
    sky_data = sorted(sky_data, key=lambda x: x[0], reverse = False)
    sky_data_xy = sky_to_image(sky_data)[:top_best]
    return sky, sky_data, sky_data_xy