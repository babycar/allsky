# -*- coding: utf-8 -*-
"""
Created on Tue Mar  3 12:06:11 2020

@author: mi
"""


import matplotlib.pyplot as plt
import numpy as np

def plot_sky_map(sky, r='dec', theta='ra', fig_in=None, theta_dir=-1, points = None):
    plt.style.use('dark_background')
    fig = fig_in if fig_in else plt.figure(figsize=(5, 5))
    ax = fig.add_axes([0.1,0.1,0.8,0.8], polar=True)
    ax.set_theta_zero_location('N')
    ax.set_rlim(90, 0)
    ax.set_theta_direction(theta_dir)
    #ax.set_ylim(0,90)
    
    if 'bright' in sky:
        ax.scatter(sky[theta], sky[r],  marker='.', c=sky['bright'], cmap='binary')
       # ax.scatter(np.asarray(sky[theta])[points], np.asarray(sky[r])[points],  marker='*', c='r', cmap='binary')
    else:
        return
        print(sky[theta], sky[r])
        ax.scatter(sky[theta], sky[r],  marker='.', alpha = 0.5)
    if not isinstance(points, type(None)):
        ax.scatter(np.asarray(sky[theta])[points], np.asarray(sky[r])[points],  marker='*', c='r', cmap='binary')

def plot_sky_image(sky_xy, y_dir=-1, text = False,  fig_in = None):
    plt.style.use('dark_background')
    fig = fig_in if fig_in else plt.figure(figsize=(5, 5))
    ax = fig.add_axes([0.1,0.1,0.8,0.8])
   # plt.gca().invert_yaxis()
    bright = [b for b, _, _ in sky_xy]
    x = [x for _, x, _ in sky_xy]
    y = [y for _, _, y in sky_xy]
    
    ax.scatter(x,y,  marker='*', color='white', cmap='binary', linewidth = 3, alpha = 0.5)
    ax.scatter(x, y,  marker='.', c=bright, cmap='binary')
    if text:
        i = 0
        for x_,y_ in zip(x,y):
            ax.text(x_,y_,str(i), fontsize = 12)
            i+=1
    
def plot_star_from_image(stars, fig_in = None):
    plt.style.use('dark_background')
    fig = fig_in if fig_in else plt.figure(figsize=(5, 5))
    ax = fig.add_axes([0.1,0.1,0.8,0.8])
    
    sizes = [s[0] for s in stars]
    max_star = max(sizes)
    min_star = min(sizes)
    for star in stars:
        x , y = star[1][0], star[1][1]
        x = x
        y = y
        c = star[0]/(max_star + min_star+10)
        ax.scatter(x, y, linewidth = 0.3,color = (c, c, c,1))