# -*- coding: utf-8 -*-
"""
Created on Thu Oct  3 18:44:31 2019

@author: Zoya
"""

import matplotlib.pyplot as plt


def plot_sky_map(sky, r='dec', theta='ra', fig_in=None, theta_dir=-1):
    plt.style.use('dark_background')
    fig = fig_in if fig_in else plt.figure(figsize=(5, 5))
    ax = fig.add_axes([0.1,0.1,0.8,0.8], polar=True)
    ax.set_theta_zero_location('N')
    ax.set_rlim(90, 0)
    ax.set_theta_direction(theta_dir)
    #ax.set_ylim(0,90)
    
    if 'bright' in sky:
        ax.scatter(sky[theta], sky[r],  marker='.', c=sky['bright'], cmap='binary')
    else:
        ax.scatter(sky[theta], sky[r],  marker='.')


def plot_sky_image(fig, sky_xy, y_dir=-1):
    plt.style.use('dark_background')
    #fig = plt.figure(figsize=(5, 5))
    ax = fig.add_axes([0.1,0.1,0.8,0.8])
    plt.gca().invert_yaxis()
    bright = [b for b, _, _ in sky_xy]
    x = [x for _, x, _ in sky_xy]
    y = [y for _, _, y in sky_xy]
    ax.scatter(y, x,  marker='.', c=bright, cmap='binary')
   # ax.show()
    
    
    
    