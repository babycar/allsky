# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

from skimage.measure import label
from skimage.measure import regionprops


def drow_circle(img, x,y, R, fill_color = 0):    
    width, height = img.shape[:2]
    img = img.copy()
    X = np.arange(width).reshape(width,1)
    Y = np.arange(height).reshape(1,height)
    mask_1 = ((X - x) ** 2 + (Y - y)**2) >= R**2
    mask_2 = ((X - x) ** 2 + (Y - y)**2) > (R-5)**2
    img[mask_1] = fill_color
    img[img<fill_color] = fill_color
    return img    

def clear_sky(sky):
    S = len(sky[sky>0])
    R = int((S/np.pi)**0.5)
    mean_color = np.mean(sky[200:300,200:300])
    width, height = sky.shape
    star_only = drow_circle(sky, width//2, height//2, R-30, fill_color = mean_color )
    step = 50
    std_all = np.std(star_only)
    std_mask = np.zeros_like(star_only)
    for i in range(0,star_only.shape[0],step):
        for j in range(0,star_only.shape[0],step):
            slice_img = star_only[i:i+step, j:j+step]
            slice_std = std_mask[i:i+step, j:j+step]
            mstd = np.std(slice_img) + (np.mean(slice_img)+np.max(slice_img))/2
           # mstd = np.std(slice_img) + (np.median(slice_img))
         #   print(np.std(slice_img))
            slice_std[slice_img>mstd] = np.std(slice_img)
            slice_img[slice_img<=mstd] = 0
            slice_img[slice_img>mstd] = 1
    star_only[star_only>mean_color] = 1
    return star_only, std_mask
    
def star_extract(star_only, std_mask, area_max = 3):
    LB = label(star_only)
    objects = regionprops(LB)

    stars = []
    for obj in objects:
        if obj.area <= area_max:        
            star_only [LB == obj.label]=0
        else:
            add = 0
            # if img is not None:
                # print(obj.centroid)
                # print(img[int(obj.centroid[0]),int(obj.centroid[1])] )
         #   print(std_mask[LB == obj.label][0])
            light_points = obj.image.shape[0]*obj.image.shape[1]+std_mask[LB == obj.label][0]/2
            stars.append([light_points, obj.centroid])
    return star_only, stars

def plot_star_from_image(stars, fig_in = None):
    plt.style.use('dark_background')
    fig = fig_in if fig_in else plt.figure(figsize=(5, 5))
    ax = fig.add_axes([0.1,0.1,0.8,0.8])
    
    sizes = [s[0] for s in stars]
    max_star = max(sizes)
    min_star = min(sizes)
    for star in stars:
        x , y = star[1][0], star[1][1]
        x = x
        y = y
        c = star[0]/(max_star + min_star+10)
        ax.scatter(x, y, linewidth = 0.3,color = (c, c, c,1))
   # plt.show()