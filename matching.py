import matplotlib.pyplot as plt
import numpy as np
import math
from scipy.spatial import distance
from plotter import plot_star_from_image, plot_sky_image, plot_sky_map
from scipy.optimize import linear_sum_assignment
from skimage.measure import label
from skimage.measure import regionprops

def coords(objects):
    coord = []
    for obj in objects:
        coord.append((0,*obj.centroid))
    return coord

def get_index(truest_stars, base_star_list):
    idx = []
    for i in range(len(base_star_list)):
        for j in range(len(truest_stars)):
           # print(base_star_list[i][1:], truest_stars[j][1:])
            if tuple(base_star_list[i][1:])==tuple(truest_stars[j][1:]):
                idx.append(i)
    return idx

def center_calculation(stars_catalog, transformed_coords, catalog_filter_idx):
    sc = stars_catalog[catalog_filter_idx]
    tc = np.asarray(transformed_coords)

    ym = (np.mean(tc[:,1]/ (100 - sc[:,2])) +  np.median(tc[:,1]/ (100 - sc[:,2])))/2
    xm =  (np.median(tc[:,0]/ (sc[:,1]))+  np.median(tc[:,0]/ (sc[:,1])))/2

    m = int((xm+ym)/2 + 0.5)
    xc, yc = stars_catalog[0][1]*xm, (100-stars_catalog[0][2])*ym

    return (xc, yc)

def star_extract_synthetic(image, values = None):
    LB = label(image)
    objects = regionprops(LB)

    stars = []
    for obj in objects:
            label_ = np.max(values[np.where(LB == obj.label)])
            print(label_)
            stars.append([obj.centroid, label_])
    sort_stars = sorted(stars, key = lambda arr:arr[1])
    print(sort_stars)
    stars = [s[0] for s in sort_stars]
    return stars

#########---from image
def coords_to_image(coords, delta):
    mxcoord = 100
    
    t = mxcoord/delta
   # print(t)
    image = np.zeros((delta,delta))
    for  coord in coords:
        #print(coord[0])
      #  print(coord)
        i, j = int(coord[1]/t), int(coord[2]/t)
       # print(i,j)
       # print(t)
        #print(i,j)
        if i == delta and j!=delta:            
            image[i-1,j] = 1
        elif j == delta and i!=delta:
            image[i,j-1] = 1
        elif i!=delta and j!=delta:
            image[i,j] = 1
        else:
            image[i-1, j-1] =1
    return image

def filter_stars_from_image(coords, x_o, y_o):
    xstart, xstop = x_o[0], x_o[1]
    ystart, ystop = y_o[0], y_o[1]
    print(x_o, y_o)
    fcoord = []
    for coord in coords:
       # print(coord)
        if xstart<coord[1]<xstop and  ystart<coord[2]<ystop:
            fcoord.append(coord)
    return fcoord

def get_piars_image(image, catalog, stars_image, stars_catalog):
#catlog, image
    image_stars = []
    catalog_stars = []
    pairs = []

    part = 5
    step = 100/part

    for i in range(part):
        for j in range(part):
            if i==j:continue
            if image[i,j] != 1 or catalog[i,j] != 1: continue
            xo = (step*i,step*(i+1))
            yo = (step*j, step*(j+1))

           # print("!!!",xo, yo)
            filtered_image_star = np.asarray(filter_stars_from_image(stars_image, xo, yo))
            filtered_catalog_star = np.asarray(filter_stars_from_image(stars_catalog, xo, yo))

            if len(filtered_image_star) < len(filtered_catalog_star):
                idxs = filter_star_indx(filtered_image_star, filtered_catalog_star)
                images = filtered_image_star
                catalogs = filtered_catalog_star[idxs]

            else:
                idxs = filter_star_indx(filtered_catalog_star, filtered_image_star)
                images = filtered_image_star[idxs]
                catalogs = filtered_catalog_star

            for cat, im in zip(images, catalogs):
                image_stars.append(im)
                catalog_stars.append(cat)
            #    print("//////")
            #    print(cat, im)
                pairs.append((cat, im))

    return pairs, image_stars, catalog_stars    
    
#########


def triangles_view(stars_):
    global origin
    medians = []
    print(len(stars_))
    for star in stars_[:len(stars_)]:
        Ms = []
        for other_star in stars_[:len(stars_)]:
            M = distance.euclidean(star[1:], other_star[1:])
            Ms.append(M)
        Ms = np.asarray(Ms)
        V = np.mean(Ms)
        medians.append(V)
#     print(medians)
#     print(medians.index(min(medians)))

    triangles = []
    for k, star in enumerate(stars_[:len(stars_)]):
        Tr = []
        for i, i_star in enumerate(stars_[:len(stars_)]):
            if k == i : continue
            for j, j_star in enumerate(stars_[:len(stars_)]):
                if k == j: continue
                s1 = i_star[1:]
                s2 = j_star[1:]
                origin = star[1:]
               # print(s1, s2)
                s1, s2 = sorted([s1,s2], key=clockwiseangle_and_distance)
              #  print(s1, s2)
                d1 = distance.euclidean(star[1:], s1)
                d2 = distance.euclidean(star[1:], s2)
                d3 = distance.euclidean(s1, s2)
                if d3!=0:
                    Tr.append((d1,d2,d3))
        triangles.append(set(Tr))
    print(len(triangles))
    return triangles

def clockwiseangle_and_distance(point):
    global origin
    refvec = [0, 1]
    # Vector between point and the origin: v = p - o
    vector = [point[0]-origin[0], point[1]-origin[1]]
    # Length of vector: ||v||
    lenvector = math.hypot(vector[0], vector[1])
    # If length is zero there is no angle
    if lenvector == 0:
        return -math.pi, 0
    # Normalize vector: v/||v||
    normalized = [vector[0]/lenvector, vector[1]/lenvector]
    dotprod  = normalized[0]*refvec[0] + normalized[1]*refvec[1]     # x1*x2 + y1*y2
    diffprod = refvec[1]*normalized[0] - refvec[0]*normalized[1]     # x1*y2 - y1*x2
    angle = math.atan2(diffprod, dotprod)
    # Negative angles represent counter-clockwise angles so we need to subtract them 
    # from 2*pi (360 degrees)
    if angle < 0:
        return 2*math.pi+angle, lenvector
    # I return first the angle because that's the primary sorting criterium
    # but if two vectors have the same angle then the shorter distance should come first.
    return angle, lenvector

def distation_matrix(star1, star2):
    M = np.zeros((len(star1), len(star2)))
    for i,trig1 in enumerate(star1):
        for j,trig2 in enumerate(star2):
            M[i,j] = distance.euclidean(trig1, trig2)
    return M

def minid(M):
    min_idx = []
    min_values = []
    for i in range(M.shape[0]):
        min_values.append(np.min(M[i]))
        min_idx.append(list(M[i]).index(min_values[i]))
    return min_idx, min_values


def filter_star_indx(original_triangles, smooth_triangles):
    indx_mask = []
    idx = 4
    M_stars = np.zeros((len(original_triangles), len(smooth_triangles)))
    for j, orig_trigs in enumerate(original_triangles):
        star_id = -1
        min_dist = 100000
        for i,star in enumerate(smooth_triangles[:]):
            M = distation_matrix(orig_trigs, star)

           # print(star)
            idx, true_idx = linear_sum_assignment(M)
            #star_minid = minid(M)
    #         print(i)
    #         print(star_minid)
    #         print(sum(star_minid[1]))
    #   #      print()
           # print(true_idx)
            minsum = np.sum(M[idx,true_idx])
            M_stars[j,i] = minsum
          ##  print(minsum)
            #print()
#             if minsum < min_dist:
#                 star_id = i
#                 min_dist = minsum
#         indx_mask.append(star_id)
 #   print(M_stars)
    idx, true_idx = linear_sum_assignment(M_stars)
    print(true_idx)
    return true_idx